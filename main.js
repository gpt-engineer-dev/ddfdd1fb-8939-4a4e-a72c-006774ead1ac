document.addEventListener("DOMContentLoaded", () => {
  const addButton = document.getElementById("addTodo");
  const inputField = document.getElementById("todoInput");
  const todoList = document.getElementById("todoList");

  addButton.addEventListener("click", () => {
    const todoText = inputField.value.trim();
    if (todoText) {
      const listItem = document.createElement("li");
      listItem.innerHTML = `
        <div class="flex items-center justify-between bg-white px-4 py-2 rounded shadow mb-2">
          <span>${todoText}</span>
          <div>
            <button class="text-green-500 hover:text-green-700"><i class="fas fa-check"></i></button>
            <button class="text-blue-500 hover:text-blue-700"><i class="fas fa-edit"></i></button>
            <button class="text-red-500 hover:text-red-700 delete-btn"><i class="fas fa-trash"></i></button>
          </div>
        </div>
      `;
      todoList.appendChild(listItem);
      inputField.value = ""; // Clear the input field

      // Add event listener to delete button
      const deleteButtons = document.querySelectorAll(".delete-btn");
      deleteButtons.forEach((button) => {
        button.addEventListener("click", (e) => {
          const listItem = e.target.closest("li");
          todoList.removeChild(listItem);
        });
      });
    }
  });

  // Additional logic for edit, delete, and mark as complete will go here
});
